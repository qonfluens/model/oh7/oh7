import geopandas as gpd
from shapely.geometry import Point, Polygon
import multiprocessing as mp
import numpy as np
from tqdm import tqdm
import georasters as gr


def dist_to_degree(lat, dxy_m):
    earth_radius = 6371e3   # m
    earth_perimeter = earth_radius * 2 * np.pi
    dlat = dxy_m * 360 / earth_perimeter
    parallel_length = earth_perimeter * np.cos(lat * np.pi / 180)
    dlon = dxy_m * 360 / parallel_length
    return dlon, dlat

def rasterize(geo, bounds_degree, resol_m):
    x0, x1, y0, y1 = bounds_degree
    dlon, dlat = dist_to_degree((y0+y1)/2, resol_m)
    NX = int((x1 - x0) / dlon)
    NY = int((y1 - y0) / dlat)
    out = np.zeros((NX, NY))
    xs = np.linspace(x0, x1, NX)
    ys = np.linspace(y0, y1, NY)
    for i, x in enumerate(tqdm(xs)):
        for j,y in enumerate(ys):
            out[i, j] = Point(x,y).within(geo.geometry[0])
    return out, xs, ys

def rasterize2(geo, bounds_degree, resol_m):
    x0, x1, y0, y1 = bounds_degree
    dlon, dlat = dist_to_degree((y0+y1)/2, resol_m)
    NX = int((x1 - x0) / dlon)
    NY = int((y1 - y0) / dlat)
    out = np.zeros((NX, NY))
    xs = np.linspace(x0, x1, NX)
    ys = np.linspace(y0, y1, NY)
    def _clip_idx(i, N):
        i = max([i, 0])
        i = min([i, N-1])
        i = max([i, 0])
        i = min([i, N-1])
        return i

    for g in geo.geometry:
        gx0, gy0, gx1, gy1 = g.bounds
        ix0 = _clip_idx(int(np.round((gx0-x0) / dlon)), NX)
        ix1 = _clip_idx(int(np.round((gx1-x0) / dlon)), NX)
        iy0 = _clip_idx(int(np.round((gy0-y0) / dlat)), NY)
        iy1 = _clip_idx(int(np.round((gy1-y0) / dlat)), NY)
        # print(f"{gx0}:{gx1}, {gy0}:{gy1} => {ix0}:{ix1}, {iy0}:{iy1}")
        for i in range(ix0, ix1+1):
            for j in range(iy0, iy1+1):
                x = x0 + i*dlon
                x_ = x + dlon
                y = y0 + j*dlat
                y_ = y + dlat
                pix = Polygon([[x, y], [x, y_], [x_, y_], [x_, y], [x, y]])
                out[i, j] += pix.intersection(g).area/pix.area
    return out, xs, ys


def load_bath(bathymetry_geotif, bounds, resol_m):
    print("loading bathymetry...")
    x0, x1, y0, y1 = bounds
    dlon, dlat = dist_to_degree((y0+y1)/2, resol_m)
    NX = int((x1 - x0) / dlon)
    NY = int((y1 - y0) / dlat)
    out = np.zeros((NX, NY))
    xs = np.linspace(x0, x1, NX)
    ys = np.linspace(y0, y1, NY)
    bath = gr.from_file(bathymetry_geotif)
    for i, x, in enumerate(xs):
        for j, y, in enumerate(ys):
            out[i,j] = bath.map_pixel(x, y) # aliasing... we'll live with it
    return out



def load_roi(beach_geojson, sea_geojson, bathymetry_geotif, bounds, resol_m):
    beach = gpd.read_file(beach_geojson)
    sea = gpd.read_file(sea_geojson)
    bathimetry_raster = load_bath(bathymetry_geotif, bounds, resol_m)
    sea_raster, xs, ys = rasterize2(sea, bounds, resol_m)
    beach_raster, _, _ = rasterize2(beach, bounds, resol_m)
    roi = {
        "beach": beach_raster,
        "sea": sea_raster,
        "bath": bathimetry_raster,
        "lon": xs,
        "lat": ys,
    }
    return sea, beach, roi
