import numpy as np


class Geodesy():
    def __init__(self, latitude):
        earth_radius = 6371e3   # m
        earth_perimeter = earth_radius * 2 * np.pi
        self.lat_degree_to_m = earth_perimeter / 360
        parallel_length = earth_perimeter * np.cos(latitude * np.pi / 180)
        self.lon_degree_to_m = parallel_length / 360


    def angular_to_metric_dist(self, dlon, dlat):
        dx = self.lon_degree_to_m * dlon
        dy = self.lat_degree_to_m * dlat
        return (dx**2 + dy**2)**.5

    def angular_to_metric_surf(self, dlon, dlat):
        dx = self.lon_degree_to_m * dlon
        dy = self.lat_degree_to_m * dlat
        return float(dx*dy)

    def metric_to_angular(self, dx, dy):
        return (dx / self.lon_degree_to_m,
                dy / self.lat_degree_to_m)
