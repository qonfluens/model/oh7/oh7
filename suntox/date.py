import numpy as np
from datetime import datetime


"""
Model an attendance index given calendar information.

NB: this might be partially / completely changed if real attendance data
is available !
"""


def date_effect(date: datetime):
    return _time_in_day(date) * _day(date) * _month(date)


def _time_in_day(date: datetime):
    hour = [0,  6,  8,    10,   12,   14,   15,   17,   19,   21,   23,  24]
    freq = [0,  0,  0.1,  0.2,  0.5,  0.9,  1.,   0.9,  0.5,  0.1,  0,   0]
    return np.interp(date.hour + date.minute / 60., hour, freq)


def _day(d: datetime):
    lo = 0.3
    hi = 1
    if d.month == 7 or d.month == 8:
        lo = 0.8
    if d >= datetime(2022,4,23) and d <= datetime(2022,5,8):
        lo = 0.8
    w = d.weekday()
    if w == 5 or w == 6:
        return hi
    if w == 2:
        return (hi+lo)/2
    return lo

def _month(d: datetime):
    # http://www.adt-herault.fr/docs/5017-2-obs-2020-herault-tourisme-chiffres-cles-pdf.pdf
    freq_per_month = np.array([1.6, 1.6, 2,   3.2,  4,   6.3,  9, 11.4,  5,  3,     2,  0])
    my_bidouille   = np.array([0,   0,   0.1, 0.3,  0.5, 1,    1,    1,  1,  0.5, 0.2,  0])
    return freq_per_month[d.month-1] * my_bidouille[d.month-1] / freq_per_month.max()
