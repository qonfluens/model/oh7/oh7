
def compute_degradation_factor(half_life_days, sim_step_min):
    N = 24 * 60 * half_life_days / sim_step_min
    return 0.5 ** (1. / N)
