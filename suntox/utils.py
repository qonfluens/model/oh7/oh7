from datetime import datetime

def tic():
    global _t0
    _t0 = datetime.now()

def toc():
    global _t0
    out = ((datetime.now() - _t0).total_seconds())
    return out
