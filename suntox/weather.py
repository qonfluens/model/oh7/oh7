from datetime import datetime
import numpy as np
from datetime import datetime
from .weather_dataset import get_weather_at

"""
Model the effect of weather on beach attendance index.

NB: this might be partially / completely changed if real attendance data
is available !
"""


def weather_effect(date: datetime):
    """ Bath Index (range [0:1]) depending on weather conditions."""
    max_tmp, rain = get_weather_at(date)
    return _rain_effect(rain) * _temp_effect(max_tmp)


def _rain_effect(rain):
    if rain == 0:
        return 1
    if rain == 1:
        return .5
    return 0


def _temp_effect(temp):
    tM = 25
    tm = 33
    if temp > tM: return 1
    if temp < tm: return 0
    return (temp - tm) / (tM - tm)
