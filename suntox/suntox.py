from datetime import datetime, timedelta
from tqdm import tqdm
import numpy as np

from .roi import load_roi
from .attendance import attendance
from .crowd import Crowd
from .spread import get_kernel, spread_iteration
from .degradation import compute_degradation_factor as deg
from .utils import tic, toc


class Simulateur():
    def __init__(self, beach_geojson, sea_geojson, bath_geotif, sim_bounds,
            **kwargs):
        dflt = {
            'start_date': "01/06/2022",
            'stop_date': "30/09/2022",
            'sim_step_minutes': 90,
            'half_life_days': 3.35, # octocrylene
            'rasters_resolution_m': 4,
            'spread_speed_sigma_mph': 20,
            'store_periodicity': 48, # write tmp file periodicity
            'store_path': '/tmp',
            'max_people_per_m2': 0.2,
        }

        self.args = {**dflt, **kwargs}

        self.t0 = datetime.strptime(self.args["start_date"], '%d/%m/%Y')
        self.t1 = datetime.strptime(self.args["stop_date"], '%d/%m/%Y')
        self.dt = timedelta(minutes=self.args["sim_step_minutes"])

        print("loading ROI...")
        self.sea, self.beach, self.rasters = \
                load_roi(beach_geojson, sea_geojson, bath_geotif, sim_bounds,
                         self.args["rasters_resolution_m"])

        self.C = Crowd(self.beach, self.sea, self.rasters,
                self.args["max_people_per_m2"],)
        self.kernel, self.mask = get_kernel(self.args['spread_speed_sigma_mph'],
                                            self.args['sim_step_minutes'] / 60.,
                                            self.args['rasters_resolution_m'],
                                            self.rasters["sea"])

        self.degradation_rate = deg(self.args['half_life_days'],
                                    self.args['sim_step_minutes'])
        self.spread_store = []
        self.timestamps_store = []
        self.swimmers_count_store = []
        self.store_idx = 0
        self.stored_rasters_count = 0

    def run(self):
        print("running simulation...")
        spread = np.zeros(self.rasters["sea"].shape)

        t = self.t0
        steps = int(np.ceil((self.t1 - self.t0) / self.dt))
        for i in tqdm(range(steps)):
            self.C.set_attendance_level(attendance(t))
            spread += self.C.spread_pulse()
            spread = spread_iteration(spread, self.kernel, self.mask)
            spread *= self.degradation_rate
            self.periodic_data_save(spread, t, self.C.count.sum())
            t += self.dt
        print("done, storing results...")
        self.periodic_data_save(spread, t-self.dt, self.C.count.sum(), force=True)
        self.post_process_saved_data()

    def model_swimmers(self, spread):
        for p in self.C.people:
            if p.wanna_goto_bath() and p.has_substance_of_interest():
                xs, ys = p.random_swim()
                mps = p.mass_spread_per_second()
                self.accumulate_substance(spread, xs, ys, mps)

    def accumulate_substance(self, raster, path_x, path_y, mass_per_second):
        for x, y in zip(path_x, path_y):
            i = np.searchsorted(self.rasters["lon"], x)
            j = np.searchsorted(self.rasters["lat"], y)
            raster[i,j] += mass_per_second

    def store_data(self):
        self.spread_store = np.array(self.spread_store)
        self.timestamps_store = np.array(self.timestamps_store)
        self.swimmers_count_store = np.array(self.swimmers_count_store)
        path = f"{self.args['store_path']}/period_{self.store_idx}"
        np.savez_compressed(path,
                spread=self.spread_store,
                timestamps=self.timestamps_store,
                swimmers_count=self.swimmers_count_store,
                allow_pickle=True
                )
        self.stored_rasters_count += len(self.spread_store)
        self.spread_store = []
        self.timestamps_store = []
        self.swimmers_count_store = []
        self.store_idx += 1

    def periodic_data_save(self, spread, t, swimmers_count, force=False):
        if t.hour == 14:
            self.spread_store.append(spread.copy())
            self.timestamps_store.append(t)
            self.swimmers_count_store.append(swimmers_count)
            if len(self.spread_store) == self.args['store_periodicity']:
                self.store_data()
            elif len(self.spread_store) > 0 and force:
                self.store_data()

    def post_process_saved_data(self):
        out_size = (self.stored_rasters_count,) + self.rasters["sea"].shape
        try:
            all_rasters = np.zeros(out_size)
        except Exception as e:
            print("An error occured, probably because this operation is too " +
                    "memory intensive. Call me ! Thomas")
            raise
        all_timestamps = [None] * self.stored_rasters_count
        all_swimmers_counts = np.zeros(self.stored_rasters_count)
        i0 = 0
        for i in range(self.store_idx):
            path = f"{self.args['store_path']}/period_{i}.npz"
            part = dict(np.load(path, allow_pickle=True))
            i1 = i0 + len(part["spread"])
            all_rasters[i0:i1, ...] = part["spread"]
            all_swimmers_counts[i0:i1] = part["swimmers_count"]
            for i in range(len(part["spread"])):
                all_timestamps[i0 + i] = part["timestamps"][i]

            i0 = i1
        # take bathymetry into account
        self.rasters["bath"] *= -1
        self.rasters["bath"][self.rasters["bath"] <= 0] = 0.5 # - won't devide by 0 !
                                            # - there are holes near the shores
                                            # - will be masked in the end anyway
        all_rasters /= self.rasters["bath"][None,...]
        # reach a known unit (after that => g/m^3)
        all_rasters /= self.C.pixel_surface_m2
        # store extrema values, to be used to scale plots later on.
        if False:
            # use some kind of hist eq, since this sim a few % of vey low values
            # (up to 1e10 lower than the rest)
            h, b = np.histogram(np.log10(all_rasters[all_rasters > 0]), 200)
            rep = np.cumsum(h) / h.sum()
            log10min = b[np.searchsorted(rep, 0.03)] # e.g. skip the 3% lowest values
            vmin = 10**log10min
        else:
            print("WARNING: vmin hardcoded (for plot scaling) !")
            vmin = 1e-9
        vmax = all_rasters.max()
        # set non-sea values to nan, so that they don't appear as 0 on the maps
        mask = np.empty(self.rasters["sea"].shape)
        mask[:] = np.nan
        mask[self.rasters["sea"] != 0] = 1
        all_rasters *= mask[None, ...]
        # save all in one single file
        path = f"{self.args['store_path']}/all.npz"
        np.savez_compressed(path, allow_pickle=True, rasters=all_rasters,
                timestamps=all_timestamps, swimmers_count=all_swimmers_counts,
                lon=self.rasters["lon"], lat=self.rasters["lat"],
                vmin=vmin, vmax=vmax)

