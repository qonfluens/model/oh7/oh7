import numpy as np
from shapely.geometry import Point
from .geodesy import Geodesy
from tqdm import tqdm

class Crowd():
    def __init__(self, beach, sea, roi, max_people_per_m2=0.2):
        self.beach = beach
        self.roi = roi
        self.sea = sea
        mean_lat = (min(beach.bounds["miny"]) + max(beach.bounds["maxy"])) / 2
        self.geodesy = Geodesy(mean_lat)
        self.max_people_per_m2 = max_people_per_m2
        self.beaches_mask = roi["beach"] != 0
        self.current_count = np.zeros(roi["beach"].shape)
        self.compute_pixel_surface()
        self.compute_closest_sea_coords()
        self.compute_spread_pulse_spec()

    def compute_closest_sea_coords(self):
        self.closest_sea_idx = np.zeros(self.beaches_mask.shape + (2,), dtype=int)
        self.bad_beaches = np.zeros(self.beaches_mask.shape)
        for i in range(self.beaches_mask.shape[0]):
            for j in range(self.beaches_mask.shape[1]):
                if self.beaches_mask[i][j]:
                    P = Point(self.roi["lon"][i], self.roi["lat"][j])
                    x0, y0 = self._closest_segment_center(P)
                    try:
                        kl = self._closest_point_idx_in_sea(x0, y0)
                        self.closest_sea_idx[i,j,:] = kl
                    except ValueError:
                        self.bad_beaches[i,j] = 1

    def _closest_segment_center(self, P):
        xx, yy = self.sea.geometry[0].geoms[0].exterior.coords.xy
        dmin = 1e15
        imin = 0
        for i in range(0, len(xx)-1):
            A = Point(xx[i], yy[i])
            B = Point(xx[i+1], yy[i+1])
            C = Point((A.x + B.x)/2, (A.y + B.y)/2)
            d = (P.x - C.x)**2 + (P.y - C.y)**2
            if d < dmin:
                imin = i
                dmin = d
        return (xx[imin] + xx[imin+1])/2, (yy[imin] + yy[imin+1])/2

    def _closest_point_idx_in_sea(self, x, y):
        i = np.searchsorted(self.roi["lon"], x)
        j = np.searchsorted(self.roi["lat"], y)
        for oi in [0, 1, -1]:
            for oj in [0, -1, 1]:
                if i+oi < 0 or i+oi >= self.roi["sea"].shape[0]:
                    continue
                if j+oj < 0 or j+oj >= self.roi["sea"].shape[1]:
                    continue
                if self.roi["sea"][i+oi,j+oj] != 0:
                    return i+oi, j+oj
        raise ValueError()

    def compute_pixel_surface(self):
        dx = self.roi["lon"][1] - self.roi["lon"][0]
        dy = self.roi["lat"][1] - self.roi["lat"][0]
        self.pixel_surface_m2 = self.geodesy.angular_to_metric_surf(dx, dy)
        self.beaches_capacity = self.roi["beach"] * self.pixel_surface_m2 * self.max_people_per_m2

    def _set_people_count(self, count):
        diff = count - self.current_count
        diff[diff < 0] = 0
        self.current_count = diff

    def compute_spread_pulse_spec(self):
        body_surface = 1 # m^2
        product_quantity = 2e-3 * body_surface * 1e4 # g
        product_concentration = 0.1
        product_frequency = 0.25
        self.mean_washed_per_indiv = product_quantity * product_frequency * product_concentration # g

    def spread_pulse(self):
        out = np.zeros(self.current_count.shape)
        for i in range(out.shape[0]):
            for j in range(out.shape[1]):
                if self.current_count[i,j]:
                    k = self.closest_sea_idx[i,j,0]
                    l = self.closest_sea_idx[i,j,1]
                    out[k,l] = self.current_count[i,j] * self.mean_washed_per_indiv
        return out

    def set_attendance_level(self, level, random_ratio=.2):
        theoretical_count = self.beaches_capacity * level
        stochasticity = np.random.normal(random_ratio, size=theoretical_count.shape)
        stochasticity *= theoretical_count
        self.count = (theoretical_count + stochasticity).round()
        self.count[self.count<0] = 0
        self._set_people_count(self.count)
