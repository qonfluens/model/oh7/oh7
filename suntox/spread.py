import numpy as np
from scipy.signal import fftconvolve

def gauss2D(shape=(3,3),sigma=0.5):
    m,n = [(ss-1.)/2. for ss in shape]
    y,x = np.ogrid[-m:m+1,-n:n+1]
    h = np.exp( -(x*x + y*y) / (2.*sigma*sigma) )
    h[ h < np.finfo(h.dtype).eps*h.max() ] = 0
    sumh = h.sum()
    if sumh != 0:
        h /= sumh
    return h

def scaled_mask(roi, kernel):
    f0 = fftconvolve(roi, kernel, mode="same")
    return np.ones(f0.shape) / (f0 + (f0==0)) * roi

def get_kernel(spread_speed_sigma_mph, # m/h
               simu_time_step, # h
               simu_spatial_resolution, # m
               roi):
    sigma = spread_speed_sigma_mph * simu_time_step / simu_spatial_resolution
    siz = int(np.ceil(3*sigma)) # huge but don't care, fftconvolve is efficient
    k = gauss2D((siz, siz), sigma)
    print("KERNEL :")
    print(f" - tgt speed {spread_speed_sigma_mph}, {simu_time_step} h/step, {simu_spatial_resolution} m/pix")
    print(f" -> size {siz}*{siz}, sigma {sigma}")
    return k, scaled_mask(roi, k)

def spread_iteration(spread, kernel, mask):
    spread = fftconvolve(spread, kernel, mode="same")
    return spread * mask
