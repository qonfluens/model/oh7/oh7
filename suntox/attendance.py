import numpy as np
from datetime import datetime
from .date import date_effect
from .weather import weather_effect

"""
"""

def attendance(date: datetime):
    return date_effect(date) * weather_effect(date)


