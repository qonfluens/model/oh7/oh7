from setuptools import setup

setup(
    name='suntox',
    version='1.0.0',
    description='OH7 - suntox project',
    packages=["suntox"],
    install_requires=["numpy", "matplotlib", "geopandas"],
)

